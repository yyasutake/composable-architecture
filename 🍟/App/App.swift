//import SwiftUI
//import Modulary
//import Favorite
//import Count
//
//#warning("I MADE THIS CLASS SINCE I CAN NOT IMPORT EMOJI MODULE IN TEST MODULE....")
//
//// struct ContentView: View {
////
////    @ObservedObject var store: Store<AppState, AppAction>
////
////    var body: some View {
////        NavigationView {
////            List {
////                NavigationLink(destination: CounterView(store: store.map(toLocalState: { (appState: AppState) -> CounterViewState in
////                     return CounterViewState(alertData: appState.alertData, counterNum: appState.counterNum, favoritePrimeList: appState.favoritePrimeList, wolframButtonEnabled: appState.wolframButtonEnabled)
////                }, toGlobalAction: { (action: CountViewAction) -> AppAction in
////                    return AppAction.countView(action)
////                 })), label: { Text("Counter") })
////                NavigationLink(destination: FavoriteView(store: store.map(toLocalState: { (appState: AppState) -> [Int] in /// WE ARE CONVERTING GLOBAL STATE TO LOCAL STATE
////                    return appState.favoritePrimeList
////                }, toGlobalAction: { (action: FavoriteAction) -> AppAction in
////                    return AppAction.favorite(action)
////                })), label: { Text("Favorite") })
////            }
////        }
////    }
////}
//
//  
//
///// THIS ENUM DESCRIBES WHOLE USE ACTION
//public enum AppAction: Equatable {
// 
//    case countView(CountViewAction)
//    case favorite(FavoriteAction)
//    case offlineCounterView(CountViewAction)
//    
//    var countView: CountViewAction? {
//      get {
//        guard case let .countView(value) = self else { return nil }
//        return value
//      }
//      set {
//        guard case .countView = self, let newValue = newValue else { return }
//        self = .countView(newValue)
//      }
//    }
//    
// 
//    var favorite: FavoriteAction? {
//      get {
//        guard case .favorite(let value) = self else { return nil }
//        return value
//      }
//      set {
//        guard case .favorite = self, let newValue = newValue else { return }
//        self = .favorite(newValue)
//      }
//    }
//    
//    
// 
//}
//
//
//
//
///// WE DO NOT NEED @PUBLISHED ANY MORE :D
///// THIS STRUCT MODELS WHOLE APP STATE AS VALUE
//public struct AppState: Equatable {
//    var counterNum: Int = 0
//    var favoritePrimeList: [Int] = []
//    var loggedInUser: User? = nil
//    var activityList: [Activity] = []
////    var alertData: AlertData?
//    var wolframButtonEnabled: Bool = true
//}
// 
//
//extension AppState {
//    
//    var countView: CounterViewState {
//        get { CounterViewState(alertData: self.alertData, counterNum: self.counterNum, favoritePrimeList: self.favoritePrimeList, wolframButtonEnabled: self.wolframButtonEnabled) }
//        set {
//            self.alertData = newValue.alertData
//            self.counterNum = newValue.counterNum
//            self.favoritePrimeList = newValue.favoritePrimeList
//            self.wolframButtonEnabled = newValue.wolframButtonEnabled
//          }
//    }
//    
//}
//
//
// 
//  
//
//struct User: Equatable {
//    let id: Int
//    let name: String
//    let bio: String
//}
//
//struct Activity: Equatable {
//    let timestamp: Date
//    let type: ActivityType
//}
//
//enum ActivityType: Equatable {
//    case addedFavoritePrime(Int)
//    case removedFavoritePrime(Int)
//}
//
//struct AppDependency {
//    var counterDependency: CounterDependency
//    var favoriteDependency: FavoriteDependency
//}
// 
//  
//func appReducer() -> ((inout AppState, AppAction, AppDependency) -> [Effect<AppAction>]) {
//    
//    return combineReducer(mapToGlobalReducer(countViewReducer, state: \.countView, action: \.countView, dependencyF: { (dependency: AppDependency) -> CounterDependency in
//        return dependency.counterDependency
//        
//    }), mapToGlobalReducer(favoriteReducer, state: \.favoritePrimeList, action: \.favorite, dependencyF: { (dependency: AppDependency) -> FavoriteDependency in
//        return dependency.favoriteDependency
//    }))
// 
//}
//
// 
//
//func higherOrderReducer(_ reducer: @escaping (inout AppState, AppAction, AppDependency) -> [Effect<AppAction>]) -> ((inout AppState, AppAction, AppDependency) -> [Effect<AppAction>]) {
//    
//    return { (state: inout AppState, action: AppAction, dependency: AppDependency) -> [Effect<AppAction>] in
//        
//        switch action {
//        
//        case .countView(.count):
//            break
//        case .countView(.checkNum(.addToFavoriteTapped)):
//            state.activityList.append(Activity(timestamp: Date(), type: ActivityType.addedFavoritePrime(state.counterNum)))
//        case .countView(.checkNum(.removeFromFavoriteTapped)):
//            state.activityList.append(Activity(timestamp: Date(), type: ActivityType.removedFavoritePrime(state.counterNum)))
//        case .favorite(.deleteFavorite(let indexSet)):
//            for index in indexSet {
//                state.activityList.append(Activity(timestamp: Date(), type: ActivityType.removedFavoritePrime(state.favoritePrimeList[index])))
//            }
//        case .favorite(.didLoadedFavoritePrimeList(_)):
//            break
//        case .favorite(.didSaveBtnTapped):
//            break
//        case .favorite(.didLoadBtnTapped):
//            break
//        case .offlineCounterView(_):
//            break
//        }
//        
//        return reducer(&state, action, dependency)
//        
//    }
//}
// 
//  
//
//
