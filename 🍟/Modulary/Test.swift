import Foundation
import Combine

public func demo() {
    
    
    //let publisher = Deferred(createPublisher: { ()-> Empty<Bool, Never> in
    //
    //    print("This is side effect")
    //    return  Empty(completeImmediately: true)
    //})
    //.eraseToAnyPublisher()

    struct _Publisher<Output>: Publisher {
        
        let publisher: AnyPublisher<Output, Never>
        
        func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
            self.publisher.receive(subscriber: subscriber)
        }
        
        typealias Failure = Never
    }

   // let _publisher: _Publisher<Bool> = _Publisher(publisher: publisher)


    /// HOW TO USE PUBLISHER AND SUBSCRIBER


    /// CREATE YOU OWN PUBLISHER
    let publisher: AnyPublisher<Bool, Never> = Deferred(createPublisher: { ()-> Just<Bool> in
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            print("This is side effect")
        })

        return  Just(true)
    })
    .eraseToAnyPublisher()


    /// SUBSCRIBER CAN GET VALUE FROM PUBLISHER
    let subscriber: AnySubscriber<Bool, Never> = AnySubscriber<Bool, Never>(receiveSubscription: { (subscription: Subscription) -> Void in
        subscription.request(.unlimited)
    }, receiveValue: { (value: Bool) -> Subscribers.Demand in
        print("value", value)
        return .unlimited
    }, receiveCompletion: { (completion: Subscribers.Completion) -> Void in
        print("completion", completion)
    })

    /// IF A SUBSCRIBER WILL BE PASSED IN THE SUBSCRIBE METHOD, WE CAN GET VALUE FROM PUBLISHER
    publisher.subscribe(subscriber)
    //value true
    //completion finished
    //This is side effect <- AFTER 2 SEC



    //let cancellable = _publisher.sink(receiveValue: { (bool: Bool) -> Void in
    //
    //})


    let sideEffectFuture = Future<Int, Never>({ (callback: @escaping (Result<Int, Never>) -> Void) -> Void in
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            callback(.success(99))
        })
    })

    /// SUBSCRIBER CAN GET VALUE FROM PUBLISHER
    let _subscriber = AnySubscriber<Int, Never>(receiveSubscription: { (subscription: Subscription) -> Void in
        subscription.request(.unlimited)
    }, receiveValue: { (value: Int) -> Subscribers.Demand in
        print("value", value)
        return .unlimited
    }, receiveCompletion: { (completion: Subscribers.Completion) -> Void in
        print("completion", completion)
    })

    /// WE PASS A SUBSCRIBER, SO THAT WE CAN GET VALUE FROM THE PUBLISHER
    //sideEffectFuture.subscribe(_subscriber)

}
