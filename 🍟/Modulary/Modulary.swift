 import Foundation
 import SwiftUI
 import Combine
 
 
 public func test() { }
 
 struct _Publisher<Output>: Publisher {
    
    let publisher: AnyPublisher<Output, Never>
    
    func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
        self.publisher.receive(subscriber: subscriber)
    }
    
    typealias Failure = Never
 }
 
 
 
 #warning("IT SHOULD BE STRUCT AS MUCH AS POSSIBLE (TO AVOID COMPLEXITY)")
 #warning("SIDE EFFECT IS NOTHING MORE THAN THE CAPSULE WHICH WILL BE EXECUTED IN THE STORE")
 
 /// STORE MANAGES ALL USER ACTION
 /// WRAP STATE AND PUBLISH CHANGES BY USING OBSERVABLE OBJECT

 
 #warning("SIDE EFFECT IS NOTHING MORE THAN THE CAPSULE WHICH WILL BE EXECUTED IN THE STORE")
 /// WE ARE JUST WRAPPING A PUBLISHER
 public struct Effect<Output>: Publisher {
    
    let publisher: AnyPublisher<Output, Never>
    
    public init(publisher: AnyPublisher<Output, Never>) {
        self.publisher = publisher
    }
    
    /// THIS WILL BE EXECUTED WHEN BESING SUBSCRIBED
    public func receive<S>(subscriber: S) where S : Subscriber, Self.Failure == S.Failure, Self.Output == S.Input {
        self.publisher.receive(subscriber: subscriber)
    }
    
    public typealias Failure = Never /// Effect never fails
 }
 
 

 #warning("こうやってhelperメソッドが足せるからeffectでラップしたのだ")
 extension Effect {
    
    /// WE ARE JUST WRAPPING A SIDE EFFECT
    public static func createDeferredPublisher(sideEffect: @escaping () -> Void) -> Deferred<Empty<Output, Never>> {
        
        return Deferred(createPublisher: { () -> Empty<Output, Never> in
            sideEffect()
            return Empty(completeImmediately: true)
        })
     }
 }
 
 extension Effect {
    
    public static func sync(sideEffect: @escaping () -> Output?) -> Deferred<Just<Output?>> {
        
        return Deferred(createPublisher: { () -> Just<Output?> in
             return Just(sideEffect())
        })
    }
 }
 
 
 /// THIS ONLY WORKS ON PUBLISHERS THAT DO NOT FAIL
 extension Publisher where Failure == Never {
    
    public func eraseToEffect() -> Effect<Output> {
        
        return Effect(publisher: self.eraseToAnyPublisher())
    }
 }
 
  
 
public class StoreWrapper<State, Action>: ObservableObject {
    
    @Published public fileprivate(set) var state: State
    
    var cancellable: Cancellable?
    
    public var send: (Action) -> Void
    
    init(state: State, send: @escaping (Action) -> Void) {
        self.state = state
        self.send = send
    }
     
 }
 

 
 /// STORE HAS GOT A WRAPPER
 /// IF THE STATE OF STORE CHANGES, THE WRAPPER GETS A NEW STATE
 
 extension Store where State: Equatable {
    
    public var storeWrapper: StoreWrapper<State, Action> {
        
        let _storeWrapper = StoreWrapper<State, Action>(state: self.state, send: self.send)
        
        #warning("counter view stateが変更されたらfavoriteが反応してもcounterも反応してしまう")

        #warning("なぜならmapでfavoriteのactionが渡されてもcounter view storeのsendが毎回呼ばれているから (-> つまり毎回counter view stateが変更されていることになる)")
        
        #warning("そしてcountのinternalも大元はcounter view stateなのでfavoriteが変更されてもcounter view stateが変わるから何も変更されていないcount internal stateもcounter view stateからやってくる")


        _storeWrapper.cancellable = self.$state
             .removeDuplicates(by: { (state01: State, state02: State) -> Bool in
                return state01 == state02
            })
            .sink(receiveValue: { [weak _storeWrapper] (state: State) -> Void in
                _storeWrapper?.state = state
                 #warning("counter viewでinternal storeはinitが終わったら破棄されてしまうのでどこかでそのinternal stateを保持しておく必要がある")
                self
            })
        
        return _storeWrapper
    }
 }
 
 public class Store<State, Action> {
    
    @Published  private(set) var state: State /// WE NEED TO ACCESS OUTSIDE OF THE MODULE
    
    /// WHEN WE COMBINE REDUCERS, WE GET MULTIPLE EFFECT, THEREFORE EFFECT SHOULD BE AN ARRAY, SO IF WE DO NOT COMBINE REDUCER, IT CAN BE WITHOUT ARRAY
    private let reducer: (inout State, Action, Any) -> [Effect<Action>] /// WE DO NOT NEED TO ACCESS THIS OUTSIDE OF THE MODULE
    
    let dependency: Any
    
    var cancellable: Cancellable?
    
    var sideEffectCancellables = Set<AnyCancellable>()
    
    public init<Dependency>(state: State, reducer: @escaping (inout State, Action, Dependency) -> [Effect<Action>], dependency: Dependency) {
        self.state = state
         self.reducer = { (state: inout State, action: Action, dependency: Any) in
            reducer(&state, action, dependency as! Dependency)
        }
        self.dependency = dependency
    }
    
    private func send(_ action: Action) {
        
        let effects: [Effect<Action>] = self.reducer(&state, action, dependency) /// REDUCER KANN EIN NEUES ACTION SPUCKEN
        
        /// ここでやりたいことはactionをまたsendで送ってreducerに実行させること
        
        effects.forEach { (effect: Effect<Action>) in
            
           // var sideEffectCancellable: AnyCancellable!
            
            
            #warning("最後にretain cycleがあるのかチェックしてそこを論文にかける")
            /// SUBSCRIBER CAN GET VALUE FROM PUBLISHER
            let subscriber: AnySubscriber<Action, Never> = AnySubscriber<Action, Never>(receiveSubscription: { (subscription: Subscription) -> Void in
                subscription.request(.unlimited)
            }, receiveValue: { [weak self] (action: Action) -> Subscribers.Demand in
                self?.send(action) /// THIS SEND IS JUST WAITING FOR A RESULT OF AN ASYNC TASK
                return .unlimited
            }, receiveCompletion: { (completion: Subscribers.Completion) -> Void in
                //print("completion", completion)
            })
            
            
            effect.subscribe(subscriber)

            /// OR USE SINK
//            sideEffectCancellable = effect.sink(receiveCompletion: { [weak self] (completion: Subscribers.Completion<Never>) -> Void in
//
//                self?.sideEffectCancellables.remove(sideEffectCancellable)
//
//            }, receiveValue: { (action: Action) -> Void in
//                self.send(action)
//            })
             //self.sideEffectCancellables.insert(sideEffectCancellable)
            
        }
 
    }
 
    public func map<LocalState, LocalAction>(toLocalState: @escaping (State) -> LocalState, toGlobalAction: @escaping (LocalAction) -> Action) -> Store<LocalState, LocalAction> {
        /// THIS PART IS REDUCER PART, SO WE NEED TO INVOLKE SEND

        
        let localStore = Store<LocalState, LocalAction>(state: toLocalState(self.state), reducer: { (localState: inout LocalState, localAction: LocalAction, dependency: Any) in
            
            self.send(toGlobalAction(localAction))  /// WE ARE CHANGING GLOBAL VALUE
            localState = toLocalState(self.state) /// WE CONVERT THE GLOBAL VALUE INTO LOCAL VALUE
            return []
        }, dependency: dependency)
        
        
        localStore.cancellable = self.$state
            .map({ (globalState: State) -> LocalState in
                return toLocalState(globalState)
            })
          
            .sink { [weak localStore] (localState: LocalState) in /// WE NEED TO OBSERVE THE CHANGE OF GLOBAL VALUE
            localStore?.state = localState
        }
        
        return localStore
        
    }
  }
 
 
 
 func test() -> () -> Void {
    
    
    return {}
 }
  
 
 /// WE NEED THIS TO COMBINE SMALL REDUCERS INTO A BIG REDUCER
 /// BY USING LOCAL STATE AND ACTION, EACH REDUCER CAN CONCENTRATE ON THEIR OWN STUFF
 
 public func combineReducer<State, Action, Dependency>(_ reducers: (inout State, Action, Dependency) -> [Effect<Action>]...) -> ((inout State, Action, Dependency) -> [Effect<Action>]) {
    
    return { ( state: inout State, action: Action, dependency: Dependency) -> [Effect<Action>] in
        
        let effects: [Effect<Action>] = reducers.flatMap { (reducer: (inout State, Action, Dependency) -> [Effect<Action>]) ->  [Effect<Action>] in
            return reducer(&state, action, dependency)
        }
        
        return effects
    }
 }
 
 
 public func mapToGlobalReducer<GlobalState, LocalState, GlobalAction, LocalAction, LocalDependency, GlobalDependency>(reducer: @escaping (inout LocalState, LocalAction, LocalDependency) -> [Effect<LocalAction>], state: WritableKeyPath<GlobalState, LocalState>, action: WritableKeyPath<GlobalAction, LocalAction?>, dependencyF: @escaping (GlobalDependency) -> (LocalDependency)) -> ((inout GlobalState, GlobalAction, GlobalDependency) -> [Effect<GlobalAction>]) {
    
    return { (globalState: inout GlobalState, globalAction: GlobalAction, globalDependency: GlobalDependency) in
        
        guard let localAction = globalAction[keyPath: action] else { return [] }
        
        let localEffects: [Effect<LocalAction>] = reducer(&globalState[keyPath: state], localAction, dependencyF(globalDependency))
 
        
        return localEffects.map { (localEffect: Effect<LocalAction>) -> Effect<GlobalAction> in
            
            localEffect.map({ (localAction: LocalAction) -> GlobalAction in
                var globalAction = globalAction
                                   globalAction[keyPath: action] = localAction
                                    return (globalAction) /// CALLBACKS WERDEN IMMER MIT GLOBAL ACTION AUSGEFUEHRT
            }).eraseToEffect()
 
        }
     }
 }


 
 /// WE USE THIS TO ADD MORE FUNCTION TO OUR REDUCER
 /// WE DO NOT WANNA POLUTE CROSS CUTTING CONCERN IN LOCAL REDUCER
 
 public func higherOrderReducerLogging<State, Action, Dependency>(_ reducer: @escaping (inout State, Action, Dependency) -> [Effect<Action>]) -> ((inout State, Action, Dependency) -> [Effect<Action>]) {
  
    return { (state: inout State, action: Action, dependency: Dependency) -> [Effect<Action>] in
        
        let effects: [Effect<Action>] = reducer(&state, action, dependency)
        let newState = state
              /// YOU CAN USE f METHOD
        return [Deferred (createPublisher: { () -> Empty<Action, Never> in
            print("Action: \(action)")
            print("State:")
            dump(newState)
            print("---")
            return Empty(completeImmediately: true)
        }).eraseToEffect()] + effects
    }
 }
 
