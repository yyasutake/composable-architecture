 import Foundation
 import SwiftUI
 import Modulary
 
 
 public struct CheckNumState: Equatable {
    
    public var counterNum: Int
    public var favoritePrimeList: [Int]
    
    public init(counterNum: Int, favoritePrimeList: [Int]) {
        self.counterNum = counterNum
        self.favoritePrimeList = favoritePrimeList
    }
 }
 
 // add dependency: Void
 public func checkNumReducer(state: inout CheckNumState, action: CheckNumAction, dependency: Void) -> [Effect<CheckNumAction>] {
    
    switch action {
    case .addToFavoriteTapped:
        state.favoritePrimeList.append(state.counterNum)
        return []
    case .removeFromFavoriteTapped:
        state.favoritePrimeList.removeAll { (num: Int) -> Bool in
            return num == state.counterNum
        }
        return []
    }
 }
 
 public enum CheckNumAction {
    case addToFavoriteTapped
    case removeFromFavoriteTapped
 }
 
 // public struct CheckNumState {
 //    public var counterNum: Int
 //    public var favoritePrimeList: [Int]
 //    public init(counterNum: Int, favoritePrimeList: [Int]) {
 //        self.counterNum = counterNum
 //        self.favoritePrimeList = favoritePrimeList
 //    }
 // }
 

 /// SINCE ALL LOGIC IS MOVED TO STORE, ALL VIEWS BECOME SIMPLE
 public struct CheckNumView: View {
    
    var store: Store<CheckNumState, CheckNumAction>
    
    @ObservedObject var storeWrapper: StoreWrapper<CheckNumState, CheckNumAction>
    
    
    public init(store: Store<CheckNumState, CheckNumAction>) {
        self.store = store
        self.storeWrapper = store.storeWrapper
    }
    
    public var body: some View {
        
        VStack {
            if isPrime(self.storeWrapper.state.counterNum) {
                Text("\(self.storeWrapper.state.counterNum) is prime")
                if self.storeWrapper.state.favoritePrimeList.contains(self.storeWrapper.state.counterNum) {
                    #warning("IF YOU WRITE LOGIC HERE, YOU CAN NOT TEST")
                    Button(action: { storeWrapper.send(.removeFromFavoriteTapped) }, label: { Text("Remove from favorite list")})
                } else {
                    Button(action: { storeWrapper.send(.addToFavoriteTapped) }, label: { Text("Add to favorite list")})
                }
            } else {
                Text("\(self.storeWrapper.state.counterNum) is not prime")
            }
        }
    }
    
    func isPrime (_ num: Int) -> Bool {
        if num <= 1 { return false }
        if num <= 3 { return true }
        for i in 2...Int(sqrtf(Float(num))) {
            if num % i == 0 { return false }
        }
        return true
    }
    
 }
