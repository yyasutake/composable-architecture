import XCTest
@testable import Count
import Combine
import Modulary
import SwiftUI
import AlertData
 
 
 
//#error("I am doing here 35:16")
/// https://www.pointfree.co/collections/composable-architecture/testing/ep84-testable-state-management-ergonomics
/// YOU SHOULD SET HOST APPLICATION TO NONE (IT IS FASTER)


class CountTests: XCTestCase {
    
    
  
  
    func test_wolframBtnTapped() {
        
        
        var dependency = CounterDependency.live
        
        let anyPublisher = Deferred(createPublisher: { () -> Just<Int?> in

             return Just(99)

        }).eraseToAnyPublisher()

        dependency.wolframAlphaSideEffect = { (num: Int) -> Effect<Int?> in

            return Effect(publisher: anyPublisher)
        }
//
        let process01 = UpdateProcess<CounterViewState, CountViewAction>(type: .send, action: CountViewAction.count(.requestWolfram), updateStateF: { (state: inout CounterViewState) -> Void in
            state.isNetworkExecuting = true
            
        })
        
        let process02 = UpdateProcess<CounterViewState, CountViewAction>(type: .receive, action: CountViewAction.count(.wolframResponse(99)), updateStateF: { (state: inout CounterViewState) -> Void in
            state.alertData = AlertData(requestNum: state.counterNum, resultNum: 99)
            state.isNetworkExecuting = false

        })
        
        let process03 = UpdateProcess<CounterViewState, CountViewAction>(type: .send, action: CountViewAction.count(.didAlertDismissBtnTapped), updateStateF: { (state: inout CounterViewState) -> Void in
            state.alertData = nil
 
        })
        
        _assert(state: CounterViewState(), updateProcesses: process01, process02, process03, reducer: countViewReducer, dependency: dependency)

        
        
    }
 
    func test_incrementTapped() throws {
        
        let dependency = CounterDependency.mock

        
        let increment_one = UpdateProcess<CounterViewState, CountViewAction>(type: .send, action: CountViewAction.count(.incrementTapped), updateStateF: { (state: inout CounterViewState) -> Void in
            state.counterNum = 3
        })
        
        let increment_two = UpdateProcess<CounterViewState, CountViewAction>(type: .send, action: CountViewAction.count(.incrementTapped), updateStateF: { (state: inout CounterViewState) -> Void in
            state.counterNum = 4
        })
        
        let decrement_one = UpdateProcess<CounterViewState, CountViewAction>(type: .send, action: CountViewAction.count(.decrementTapped), updateStateF: { (state: inout CounterViewState) -> Void in
            state.counterNum = 3
        })
        
 
        
        _assert(state: CounterViewState(counterNum: 2), updateProcesses: UpdateProcess(type: .send, action: CountViewAction.count(.decrementTapped), updateStateF:{ $0.counterNum = 1 }), reducer: countViewReducer, dependency: dependency)
        
        
        _assert(state: CounterViewState(counterNum: 2), updateProcesses: increment_one, increment_two, decrement_one, reducer: countViewReducer, dependency: dependency)
        
        /// ---------------------
         
        var state = CounterViewState(counterNum: 2)
        
        let effects = countViewReducer(&state, .count(.incrementTapped), dependency)
        
        /// BECAUSE WE CAN COPY VALUES, VALUE TYPE IS BETTAR OVER REFERENCE TYPE
        var expectedState = state
        
        expectedState.counterNum = 3
        
        XCTAssertEqual(state,  expectedState)
        
        XCTAssert(effects.isEmpty)
  
    }
    
    func test_decrementTapped() throws {
        
        let dependency = CounterDependency.mock

 
 
        var state = CounterViewState(counterNum: 2)
        
        let effects = countViewReducer(&state, .count(.decrementTapped), dependency)
        
        var expectedState = state
        
        expectedState.counterNum = 1
        
        XCTAssertEqual(state, expectedState)
        
        XCTAssert(effects.isEmpty)
  
    }
    
    func test_wolframResponse() throws {
        
        let dependency = CounterDependency.mock

        var  state = CounterViewState(alertData: nil, counterNum: 2, favoritePrimeList: [3, 5], isNetworkExecuting: false, isModalShown: false)
 
 
        var effects = countViewReducer(&state, .count(.requestWolfram), dependency)
        
        var expectedState = state
        
        expectedState.isNetworkExecuting = true
        
        XCTAssertEqual(state,  expectedState)
        
        XCTAssertEqual(effects.count, 1)
        
        effects = countViewReducer(&state, .count(.wolframResponse(3)), dependency)
        
        expectedState.alertData = AlertData(requestNum: expectedState.counterNum, resultNum: 3)
        
        expectedState.isNetworkExecuting = false

        
        XCTAssertEqual(state, expectedState)

        

  
    }
    
    
    func test_wolframResponse_nil() {
        
        let dependency = CounterDependency.mock

        var  state = CounterViewState(alertData: nil, counterNum: 2, favoritePrimeList: [3, 5], isNetworkExecuting: false, isModalShown: false)
        
 
        
     

        var effects = countViewReducer(&state, .count(.requestWolfram), dependency)
        
       

        
        var expectedState = state
        
        expectedState.isNetworkExecuting = true
        
        XCTAssertEqual(state,  expectedState)
     
      XCTAssertEqual(effects.count, 1)

        effects = countViewReducer(&state, .count(.wolframResponse(nil)), dependency)
        
        expectedState.isNetworkExecuting = false
        
        XCTAssertEqual(state,  expectedState)

      XCTAssert(effects.isEmpty)
    }
    
    
    func test_checkNum() {
        
        
        let dependency = CounterDependency.mock

        var  state = CounterViewState(alertData: nil, counterNum: 2, favoritePrimeList: [3, 5], isNetworkExecuting: false, isModalShown: false)
 
        
        var effects = countViewReducer(&state, .checkNum(.addToFavoriteTapped), dependency)

        
        XCTAssert(effects.isEmpty)
        
        var expectedState = state
        
        expectedState.favoritePrimeList = [3, 5, 2]

        XCTAssertEqual(state, expectedState)
        
         effects = countViewReducer(&state, .checkNum(.removeFromFavoriteTapped), dependency)

        
        expectedState.favoritePrimeList = [3, 5]

        XCTAssertEqual(state, expectedState)
        XCTAssert(effects.isEmpty)
        
        
        
     }
    
    func test_wolfram_tap_to_response_api_success() throws {

        let dependency = CounterDependency.mock

        var  state = CounterViewState(alertData: nil, counterNum: 2, favoritePrimeList: [3, 5], isNetworkExecuting: false, isModalShown: false)
         

        var effects = countViewReducer(&state, .count(.requestWolfram), dependency)
        
        
        var expectedState = state
        
        expectedState.isNetworkExecuting = true
        
        XCTAssertEqual(state, expectedState)


        XCTAssertEqual(effects.count, 1)
        
        var nextAction: CountViewAction!
        
        let expectation = self.expectation(description: "received completion")
        
        /// BY USING SINK WE CAN TEST SIDE EFFECTS
        let cancellation  = effects[0].sink(receiveCompletion: { (completion: Subscribers.Completion<Never>) -> Void in
           
            expectation.fulfill()
            
        }, receiveValue: { (action: CountViewAction) -> Void in
            
            XCTAssertEqual(action, CountViewAction.count(.wolframResponse(17)))
            
            nextAction = action
        })

        
        self.wait(for: [expectation], timeout: 3)

        effects = countViewReducer(&state, nextAction, dependency)
        
        expectedState.alertData = AlertData(requestNum: expectedState.counterNum, resultNum: 17)
        
        expectedState.isNetworkExecuting = false
        
         XCTAssertEqual(state, expectedState)
       
        
        XCTAssert(effects.isEmpty)

        effects = countViewReducer(&state, .count(.didAlertDismissBtnTapped), dependency)
        
        expectedState.alertData = nil

        XCTAssertEqual(state, expectedState)
      
        XCTAssert(effects.isEmpty)
 
        
    }
    
    func test_wolfram_tap_to_response_api_fail() {
        
        var dependency = CounterDependency.mock

 
        dependency.wolframAlphaSideEffect = { (counterNum: Int) -> Effect<Int?> in
 
                   let anyPublisher = Deferred(createPublisher: { () -> Just<Int?> in
                       
                       return Just(nil)
                   }).eraseToAnyPublisher()
                   
                   return Effect<Int?>(publisher: anyPublisher)
        }
        
        
        
        var  state = CounterViewState(alertData: nil, counterNum: 2, favoritePrimeList: [3, 5], isNetworkExecuting: false, isModalShown: false)
        
 
        var effects = countViewReducer(&state, .count(.requestWolfram), dependency)
        
        var expectedState = state
        
        expectedState.isNetworkExecuting = true
        
        XCTAssertEqual(state, expectedState)
    
        XCTAssertEqual(effects.count, 1)
        
        var nextAction: CountViewAction!
        
        let expectation = self.expectation(description: "received completion")
        
        let cancellable = effects[0].sink(receiveCompletion: { (completion: Subscribers.Completion<Never>) -> Void in
            
            expectation.fulfill()
            
        }, receiveValue: { (action: CountViewAction) -> Void in
            
            XCTAssertEqual(action, CountViewAction.count(.wolframResponse(nil)))
            nextAction = action
        })
        
        self.wait(for: [expectation], timeout: 3)
        
        effects = countViewReducer(&state, nextAction, dependency)
        
        expectedState.isNetworkExecuting = false

        
        XCTAssertEqual(state, expectedState)
        
        XCTAssert(effects.isEmpty)
    
    }
    
    
    
 }


