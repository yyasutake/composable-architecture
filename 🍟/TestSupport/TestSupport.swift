 
 import Foundation
 import Combine
 import Modulary
 import XCTest
 
 #warning("WHEN YOU CREATE TEST FILE, THE FILE SHOULD BE UNIT TEST SWIFT FILE (NOT SIMPLE SWIFT FILE))")
 
 
 var cancellable = Set<AnyCancellable>()
  
 public func _assert<State: Equatable, Action, Dependency>(state: State, updateProcesses: UpdateProcess<State, Action>..., reducer: (inout State, Action, Dependency) -> [Effect<Action>], dependency: Dependency) {
     
     var state = state
     
     var effects = [Effect<Action>]()

     
 //    actions.forEach({ (action: Action, updateState: (inout State) -> Void, file: StaticString, line: UInt) in
     updateProcesses.forEach({ (updateProcess: UpdateProcess) in

     var expectedState = state
  
         switch updateProcess.type {
         
         case .send:
  
            if !effects.isEmpty { XCTFail("", file: updateProcess.file, line: updateProcess.line) }
             
             effects.append(contentsOf: reducer(&state, updateProcess.actionToBePassed, dependency))

         case .receive:
             
             guard !effects.isEmpty else { XCTFail("", file: updateProcess.file, line: updateProcess.line); break }


             let effect = effects.removeFirst()
             var nextAction: Action!
             let expectation = XCTestExpectation(description: "completion")
             effect.sink(receiveCompletion: { (completion: Subscribers.Completion<Never>) -> Void in
                 expectation.fulfill()
                 
             }, receiveValue: { (action: Action) -> Void in
                 nextAction = action
             }).store(in: &cancellable)
             
             if XCTWaiter.wait(for: [expectation], timeout: 5) != .completed { XCTFail("Time out", file: updateProcess.file, line: updateProcess.line ) }
             
             XCTAssertEqual(nextAction, updateProcess.actionToBePassed, file: updateProcess.file, line: updateProcess.line)
             
             effects.append(contentsOf: reducer(&state, updateProcess.actionToBePassed, dependency))

         }
         
         updateProcess.updateExpectedState(&expectedState)
         XCTAssertEqual(state, expectedState, file: updateProcess.file, line: updateProcess.line) /// WE ADD FIEL AND LINE SO THAT THE ERROR WILL BE SHOWN ON THE CALLED METHOD
         

         
     })
 }


public enum UpdateType {
   case send
   case receive
 }
  
 /// THIS SHOULD BE UPDATE EXPECTED STATE??

public struct UpdateProcess<State, Action: Equatable> {
     
     let actionToBePassed: Action
     let updateExpectedState: (inout State) -> Void
     let file: StaticString
     let line: UInt
     let type: UpdateType
     
     init(type: UpdateType, action: Action, updateStateF: @escaping (inout State) -> Void, file: StaticString = #file, line: UInt = #line) {
         self.actionToBePassed = action
         self.updateExpectedState = updateStateF
         self.file = file
         self.line = line
         self.type = type
     }
      
 }


