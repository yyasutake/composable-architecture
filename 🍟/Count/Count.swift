 import Foundation
 import SwiftUI
 import Modulary
 import CheckNum
 import Combine
 import AlertData
 
 
// func dataTask(url: URL) -> Effect<(Data?, URLResponse?, Error?)> {
//    
//    return Effect.init(sideEffect: { (callback: @escaping ((Data?, URLResponse?, Error?)) -> Void) -> Void in
//        URLSession.shared.dataTask(with: url) { data, response, error in
//          callback((data, response, error))
//        }
//        .resume()
//    })
// }
// 
// 
//
// 
// extension Effect where A == (Data?, URLResponse?, Error?) {
//    
//    func decode<B: Decodable>(type: B.Type) -> Effect<B?> {
//        return self.map(aToB: { (dataTaskResult: (data: Data?, response: URLResponse?, error: Error?)) -> B? in
//            let result = dataTaskResult.data.flatMap { (data: Data) -> B? in
//                return try? JSONDecoder().decode(B.self, from: data)
//            }
//            return result
//        })
//    }
// }
// 
// extension Effect {
//    
//    func receive(queue: DispatchQueue) -> Effect {
//        return Effect.init(sideEffect: { (callback: @escaping (A) -> Void) -> Void in
//            self.sideEffect({ (a: A) in queue.async { callback(a) } })
//        })
//    }
//    
// }
 
 
 
 
 #warning("IF THERE IS A NEW API, WE CAN CHECK IT OUT")
 func offlineMethod(num: Int) -> Effect<Int?> {
    
    let pub = Future({ (callback: Future<Int?, Never>.Promise) -> Void in
        
        callback(.success(100))
    })
    .eraseToAnyPublisher()
    
    return Effect(publisher: pub)
 }
 

 
 func wolframAlphaSideEffect(num: Int) -> Effect<Int?> {
    
    let anyPublisher = askForWolframAlpha(queryString: "prime \(num)").map ( { (wolframAlphaResult: WolframAlphaResult?) -> Int? in
        
        let resultInt = wolframAlphaResult.flatMap { (result: WolframAlphaResult) -> String? in
            result.queryresult.pods.first(where: { (pod: Pod) -> Bool in
                return pod.primary == .some(true) /// YOU CAN ALSO WRITE JUST ONLY true
            })?.subpods.first?.plaintext
        }.flatMap { (numString: String) -> Int? in
            return Int.init(numString)
        }
         return resultInt
    }).eraseToAnyPublisher()
    
    return Effect(publisher: anyPublisher)
 }
  
 func askForWolframAlpha(queryString: String) -> Effect<WolframAlphaResult?> {
    
    var components = URLComponents(string: "https://api.wolframalpha.com/v2/query")!
    
    components.queryItems = [
        URLQueryItem(name: "input", value: queryString),
        URLQueryItem(name: "format", value: "plaintext"),
        URLQueryItem(name: "output", value: "JSON"),
        URLQueryItem(name: "appid", value: "6H69Q3-828TKQJ4EP"),
    ]
    
    let anyPublisher = URLSession.shared.dataTaskPublisher(for: components.url(relativeTo: nil)!)
        .map({ (data: Data, response: URLResponse) -> Data in
            return data
        })
        .decode(type: WolframAlphaResult?.self, decoder: JSONDecoder())
        .replaceError(with: nil)
        .eraseToAnyPublisher()
    
    return Effect(publisher: anyPublisher)

 
 }
 
  

 public struct CounterDependency {
    
    public var wolframAlphaSideEffect: (Int) -> Effect<Int?>
 
 }
 
 
 extension CounterDependency {
    
    public static let live = CounterDependency.init(wolframAlphaSideEffect: wolframAlphaSideEffect(num:))
    
    public static let liveOffline = CounterDependency.init(wolframAlphaSideEffect: offlineMethod(num:))
    
    
 }


 #if DEBUG
 extension CounterDependency {
    
    public static let mock = CounterDependency(wolframAlphaSideEffect: { (counterNum: Int) -> Effect<Int?> in
        
        
               let anyPublisher = Deferred(createPublisher: { () -> Just<Int?> in
                   
                   return Just(17)
               }).eraseToAnyPublisher()
               
               return Effect<Int?>(publisher: anyPublisher)
        
    })
 }
 #endif
 
 #warning("STATE")
 /// WE USE Equatable FOR TESTING (FOR XCTAssertEquals)
 public struct CounterViewState: Equatable {
 
    public var alertData: AlertData?
    public var counterNum: Int
    public var favoritePrimeList: [Int] /// WE ONLY NEED THIS FOR CHECK NUM VIEW
    public var isNetworkExecuting: Bool
    public var isModalShown: Bool
 
    #warning("FOR TEST WE CAN SET DEFAULT VALUE :D")
    
    public init(alertData: AlertData? = nil, counterNum: Int = 0, favoritePrimeList: [Int] = [], isNetworkExecuting: Bool = false, isModalShown: Bool = false) {
        self.alertData = alertData
        self.counterNum = counterNum
        self.favoritePrimeList = favoritePrimeList
        self.isNetworkExecuting = isNetworkExecuting
        self.isModalShown = isModalShown
    }
    
    var counterState: (counterNum: Int, alertData: AlertData?, isNetworkExecuting: Bool, isModalShown: Bool) {
        get { (self.counterNum, self.alertData, self.isNetworkExecuting, self.isModalShown) }
        set { (self.counterNum, self.alertData, self.isNetworkExecuting, self.isModalShown) = newValue }
    }
    
    var checkNumState: CheckNumState {
        get { CheckNumState(counterNum: self.counterNum, favoritePrimeList: self.favoritePrimeList) }
         set { self.counterNum = newValue.counterNum
            self.favoritePrimeList = newValue.favoritePrimeList }
    }
    
 }
 
 #warning("Action")
 public enum CountAction: Equatable {
    case decrementTapped
    case incrementTapped
    case wolframResponse(Int?)
    case didAlertDismissBtnTapped
    case didCheckNumBtnTapped
    case didModalDismissed
    
//    case doubleTap
//    case wolframBtnTapped
    case requestWolfram
    
 }
 
 public enum CountViewAction: Equatable {
    
    case count(CountAction)
    case checkNum(CheckNumAction)
    
    
    public  var count: CountAction? {
        get {
            guard case let .count(value) = self else { return nil }
            return value
        }
        set {
            guard case .count = self, let newValue = newValue else { return }
            self = .count(newValue)
        }
    }
     public var checkNum: CheckNumAction? {
        get {
            guard case let .checkNum(value) = self else { return nil }
            return value
        }
        set {
            guard case .checkNum = self, let newValue = newValue else { return }
            self = .checkNum(newValue)
        }
    }
 }
 
 
 /// IF YOU USE STRUCT HERE, YOU CAN NOT USE KEY PATH
 /// COUNT VIEW AND CHECK NUM VIEW SHOULD HAVE SAME STATE (BUT STRUCT CAN NOT BE USED BECAUSE OF KEY PATH, OTHERWISE YOU CAN USE TUPLE BUT I DO NOT LIKE TO USE TUPLE BECAUSE OF REALABILITY)
 
 public let countViewReducer: (inout CounterViewState, CountViewAction, CounterDependency) -> [Effect<CountViewAction>] = combineReducer(mapToGlobalReducer(countReducer, state: \.counterState, action: \.count, dependencyF: { (dependency: CounterDependency) -> CounterDependency in
    return dependency
    
 }), mapToGlobalReducer(checkNumReducer, state: \.checkNumState, action: \.checkNum, dependencyF: { (dependency: CounterDependency) -> () in
    return {}()
 }))
// public let countViewReducer: (inout CounterViewState, CountViewAction) -> [Effect<CountViewAction>] = combineReducer(executeLocalFunction(countReducer, state: \CounterViewState.counterState, action: \.count))

 

 
 /// EACH REDUCER IS RESPONSIBLE FOR EACH SCREEN
 
 #warning("株の全部を表示する")
 
 #warning("シグナルをお知らせする")
 
 
 public func countReducer(state: inout (counterNum: Int, alertData: AlertData?, isNetworkExecuting: Bool, isModalShown: Bool), action: CountAction, dependency: CounterDependency) -> [Effect<CountAction>] {
    
    switch action {
    
    case .decrementTapped:
        state.counterNum -= 1
        return []
        
    case .incrementTapped:
        state.counterNum += 1
 //        let pub = Deferred(createPublisher: { () -> Publishers.Delay<Just<CountAction>, DispatchQueue> in
//            print("♦️ Yoooo")
//            return Just<CountAction>(.decrementTapped).delay(for: 1, scheduler: DispatchQueue.main)
//        }).eraseToAnyPublisher()
//
//        let effect = Effect<CountAction>(publisher: pub)
//        return [effect]
        return []
        
        
    case .requestWolfram:
        state.isNetworkExecuting = true
 //        /// callbackはsendのこと
//        return [getAnswerFromWolframAlpha(num: count).map(aToB: { (num: Int?) -> CountAction in
//            return  CountAction.wolframResponse(num)
//        })]
        
        #warning("REDUCER DELEGATES MESSY EXECUTION OF SIDE EFFECTS TO THE STORE")
        
        
        
        let anyPublisher = dependency.wolframAlphaSideEffect(state.counterNum)
            .map({ (num: Int?) -> CountAction in
                return CountAction.wolframResponse(num)
            })
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
 
//        let anyPublisher = getSideEffectPublisherOfWolframAlpha(num: count)//.map(CountAction.wolframResponse)
//            .map({ (num: Int?) -> CountAction in
//            return .wolframResponse(num)
//            })
//            .receive(on: DispatchQueue.main)
//            .eraseToAnyPublisher()
        
        let effect = Effect<CountAction>(publisher: anyPublisher)
        return [effect]
 
        
    case .wolframResponse(let response):
        state.alertData = response.map({ (num: Int) -> AlertData in
            return AlertData(requestNum: state.counterNum, resultNum: num)
        })
        state.isNetworkExecuting = false
        return []
        
    case .didAlertDismissBtnTapped:
        state.alertData = nil
        return []
    case .didCheckNumBtnTapped:
        state.isModalShown = true
        return []
    case .didModalDismissed:
        state.isModalShown = false
        return []
    }
 }
 
 
 /// INCR TAPPED
 /// STATE CHANGED
 ///
 public struct CounterView: View {
    
    
    #warning("UI IST ZUSTAENDIG FUER INTERNAL LOGIC")
     var store: Store<CounterViewState, CountViewAction> /// NOW OUR VIEW TAKE SMALLER STATE THAN APP STATE
    
    
    #warning("STORE WRAPPER IST NUR ZUSTAENDIG FUER UI")
    #warning("THIS OBJECT SENDS ACTION, SO THAT INTERNAL LOGIC WILL BE EXECUTED")
     @ObservedObject var storeWrapper: StoreWrapper<InternalState, InternalAction>
    
    /// WE DO NOT NEED THIS ANYMORE
   // @State var isModalShown: Bool = false
    
 
    struct InternalState: Equatable {
        
        var wolframButtonEnabled: Bool
        var isModalShown: Bool
        var alertData: AlertData?
        var counterNum: Int
        
        var isIncrementBtnEnabled: Bool {
            return wolframButtonEnabled
        }
        
        var isDecrementBtnEnabled: Bool {
            return wolframButtonEnabled
        }
       
    }
    
    /// WE ONLY DEFINE WHAT USER DO ON THE VIEW
    
    public enum InternalAction: Equatable {
       case decrementTapped
       case incrementTapped
     //  case wolframResponse(Int?) <- WE DO NOT WANNA THE USER TO USE THIS ACTION
       case didAlertDismissBtnTapped
       case didCheckNumBtnTapped
       case didModalDismissed
       
       case doubleTap
       case wolframBtnTapped
 
    }
  
    
    public init(store: Store<CounterViewState, CountViewAction>) {
        self.store = store
 
      
        /// WE ARE CONVERTING THE STORE INTO THE STORE WHICH IS ONLY APPROPRIATE FOR COUNTER VIEW
        
        let internalStore: Store<CounterView.InternalState, CounterView.InternalAction> = store.map(toLocalState: { (globalState: CounterViewState) -> InternalState in
            
            return InternalState(wolframButtonEnabled: !globalState.isNetworkExecuting, isModalShown: globalState.isModalShown, alertData: globalState.alertData, counterNum: globalState.counterNum)
            
        }, toGlobalAction: { (localAction: InternalAction) -> CountViewAction in
            
            switch localAction {
            
            case .decrementTapped:
                return CountViewAction.count(.decrementTapped)
            case .incrementTapped:
                return CountViewAction.count(.incrementTapped)
          
            case .didAlertDismissBtnTapped:
                return CountViewAction.count(.didAlertDismissBtnTapped)
            case .didCheckNumBtnTapped:
                return CountViewAction.count(.didCheckNumBtnTapped)
            case .didModalDismissed:
                return CountViewAction.count(.didModalDismissed)
            case .doubleTap:
                return CountViewAction.count(.requestWolfram)
            case .wolframBtnTapped:
                return CountViewAction.count(.requestWolfram)
            }
         })
        
 
        self.storeWrapper = internalStore.storeWrapper
         
    }
    
    
    public var body: some View {
        
        print("count view")
        return VStack {
            HStack {
                Button(action: { storeWrapper.send(.decrementTapped) }, label: { Text("-") }).disabled(!storeWrapper.state.isDecrementBtnEnabled)
                Text("\(storeWrapper.state.counterNum)")
                Button(action: { storeWrapper.send(.incrementTapped) }, label: { Text("+") }).disabled(!storeWrapper.state.isIncrementBtnEnabled)
            }
            Button(action: { storeWrapper.send(.didCheckNumBtnTapped) }, label: { Text("Is this prime?") })
            Button(action: { storeWrapper.send(.wolframBtnTapped) }, label: { Text("What is the \(self.storeWrapper.state.counterNum)th prime?") }).disabled(!storeWrapper.state.wolframButtonEnabled)
            // YOU COULD ALSO CREATE A TITLE STATE
        }
        
        .sheet(
          isPresented: Binding.constant(self.storeWrapper.state.isModalShown),
          onDismiss: { storeWrapper.send(.didModalDismissed) }
        ) {
            CheckNumView(store: self.store.map(toLocalState: { (state: CounterViewState) -> CheckNumState in
                 return CheckNumState(counterNum: state.counterNum, favoritePrimeList: state.favoritePrimeList)
                /// IF YOU USE ALIAS, YOU DO NOT NEED TO DO THIS
            }, toGlobalAction: { (action: CheckNumAction) -> CountViewAction in
                return CountViewAction.checkNum(action)
            }))
        }
        
  
        
        /// OR WE CAN WRITE Binding.constant(self.store.value.alertNthPrime)
             .alert(item: Binding(get: { self.storeWrapper.state.alertData }, set: { _ in })) { (alertData: AlertData) in
                
                 return Alert(title: Text("The \(alertData.requestNum) prime is \(alertData.requestNum)"), dismissButton: .default(Text("Ok")) {
                    storeWrapper.send(.didAlertDismissBtnTapped)
               })
         }
        
        
        
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity) // FILLS ENTIRE SCREEN
        .background(Color.white) // TO INFORM THE VIEW THAT THERE IS SOME ELEMENT HERE
        .onTapGesture(count: 2, perform:  { () -> Void in
            storeWrapper.send(.doubleTap)
        })
        
    }
     
    
 }
 
 

 
 
 
 
 struct WolframAlphaResult: Decodable {
    let queryresult: QueryResult
 }
 
 struct QueryResult: Decodable {
    let pods: [Pod]
 }
 
 struct Pod: Decodable {
    let primary: Bool?
    let subpods: [SubPod]
 }
 
 struct SubPod: Decodable {
    let plaintext: String
 }
 
